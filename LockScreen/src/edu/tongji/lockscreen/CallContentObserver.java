package edu.tongji.lockscreen;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.CallLog.Calls;
import android.util.Log;

public class CallContentObserver extends ContentObserver {

	private static final String TAG = "MissedCallContentObserver";
	private Context context;

	public CallContentObserver(Context context, Handler handler) {
		super(handler);
		this.context = context;
	}

	@Override
	public void onChange(boolean selfChange) {

		Cursor cursor = context.getContentResolver().query(Calls.CONTENT_URI,
				new String[] { Calls.NUMBER, Calls.TYPE, Calls.NEW }, null,
				null, Calls.DEFAULT_SORT_ORDER);

		if (cursor != null) {
			if (cursor.moveToFirst()) {
				int type = cursor.getInt(cursor.getColumnIndex(Calls.TYPE));
				switch (type) {
				case Calls.MISSED_TYPE:
					Log.v(TAG, "missed type");
					if (cursor.getInt(cursor.getColumnIndex(Calls.NEW)) == 1) {
						Log.v(TAG, "you have a missed call");
					}
					break;
				case Calls.INCOMING_TYPE:
					Log.v(TAG, "incoming type");
					break;
				case Calls.OUTGOING_TYPE:
					Log.v(TAG, "outgoing type");
					break;
				}
			}
			cursor.close();
		}
	}

	@Override
	public boolean deliverSelfNotifications() {
		return super.deliverSelfNotifications();
	}
}
