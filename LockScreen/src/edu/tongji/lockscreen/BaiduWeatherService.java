package edu.tongji.lockscreen;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.location.Location;
import android.util.Log;

public class BaiduWeatherService {

	private static String ak = "BjU61pdhGZdnB16jS7TsN6ZG";
	private static String TAG = "BaiduWeatherService";
	private static String urlAPI = "http://api.map.baidu.com/telematics/v3/weather?location=";
	
	private BaiduWeatherInfo weatherInfo = new BaiduWeatherInfo();

	public BaiduWeatherInfo getWeatherInfo() {
		return weatherInfo;
	}

	public void getWeatherInfo(Location location) {
		if (location != null) {
			String uri = urlAPI + location.getLongitude() + ","
					+ location.getLatitude() + "&output=json&ak=" + ak;
			HttpGet httpRequest = new HttpGet(uri);
			try {
				HttpResponse httpResponse = new DefaultHttpClient()
						.execute(httpRequest);
				if (httpResponse.getStatusLine().getStatusCode() == 200) {
					String result = EntityUtils.toString(httpResponse
							.getEntity());
					weatherInfo.selfParse(result);
					Log.i(TAG, weatherInfo.toString());
				}
			} catch (Exception e) {
				Log.i(TAG, e.toString());
			};
		}
	}
}
