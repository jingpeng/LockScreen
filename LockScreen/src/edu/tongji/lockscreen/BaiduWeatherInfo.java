package edu.tongji.lockscreen;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class BaiduWeatherInfo {

	private static final String TAG = "BaiduWeatherInfo";

	private static final String errorTag = "error";
	private static final String statusTag = "status";
	private static final String dateTag = "date";
	private static final String resultsTag = "results";

	private int error;
	private String status;
	private String date;
	
	private List<EachCityWeatherInfo> cityWeatherInfoList = new ArrayList<EachCityWeatherInfo>();

	public List<EachCityWeatherInfo> getCityWeatherInfoList() {
		return cityWeatherInfoList;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void selfParse(String result) {
		try {
			JSONObject weatherObject = new JSONObject(result);
			setError(weatherObject.getInt(errorTag));
			setStatus(weatherObject.getString(statusTag));
			setDate(weatherObject.getString(dateTag));

			JSONArray resultsArray = weatherObject.getJSONArray(resultsTag);
			for (int i = 0; i != resultsArray.length(); i++) {
				JSONObject eachCityResult = resultsArray.getJSONObject(i);
				EachCityWeatherInfo eachCityWeatherInfo = new EachCityWeatherInfo();
				eachCityWeatherInfo.selfParse(eachCityResult);
				cityWeatherInfoList.add(eachCityWeatherInfo);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	class EachCityWeatherInfo {

		private static final String currentCityTag = "currentCity";
		private static final String weatherDataTag = "weather_data";

		private String currentCity;
		private List<EachDetailWeatherInfo> detailWeatherInfoList = new ArrayList<EachDetailWeatherInfo>();

		public List<EachDetailWeatherInfo> getDetailWeatherInfoList() {
			return detailWeatherInfoList;
		}

		public String getCurrentCity() {
			return currentCity;
		}

		public void setCurrentCity(String currentCity) {
			this.currentCity = currentCity;
		}

		public void selfParse(JSONObject eachCityResult) {
			try {
				setCurrentCity(eachCityResult.getString(currentCityTag));
				JSONArray weatherDataArray = eachCityResult
						.getJSONArray(weatherDataTag);
				for (int i = 0; i != weatherDataArray.length(); i++) {
					JSONObject eachWeatherData = weatherDataArray
							.getJSONObject(i);
					EachDetailWeatherInfo eachDetailWeatherInfo = new EachDetailWeatherInfo();
					eachDetailWeatherInfo.selfParse(eachWeatherData);
					detailWeatherInfoList.add(eachDetailWeatherInfo);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	class EachDetailWeatherInfo {

		private static final String dateTag = "date";
		private static final String dayPictureUrlTag = "dayPictureUrl";
		private static final String nightPictureUrlTag = "nightPictureUrl";
		private static final String weatherTag = "weather";
		private static final String windTag = "wind";
		private static final String temperatureTag = "temperature";

		private String date;
		private String dayPictureUrl;
		private String nightPictureUrl;
		private String weather;
		private String wind;
		private String temperature;

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getDayPictureUrl() {
			return dayPictureUrl;
		}

		public void setDayPictureUrl(String dayPictureUrl) {
			this.dayPictureUrl = dayPictureUrl;
		}

		public String getNightPictureUrl() {
			return nightPictureUrl;
		}

		public void setNightPictureUrl(String nightPictureUrl) {
			this.nightPictureUrl = nightPictureUrl;
		}

		public String getWeather() {
			return weather;
		}

		public void setWeather(String weather) {
			this.weather = weather;
		}

		public String getWind() {
			return wind;
		}

		public void setWind(String wind) {
			this.wind = wind;
		}

		public String getTemperature() {
			return temperature;
		}

		public void setTemperature(String temperature) {
			this.temperature = temperature;
		}

		public void selfParse(JSONObject eachWeatherResult) {
			try {
				setDate(eachWeatherResult.getString(dateTag));
				setDayPictureUrl(eachWeatherResult.getString(dayPictureUrlTag));
				setNightPictureUrl(eachWeatherResult
						.getString(nightPictureUrlTag));
				setWeather(eachWeatherResult.getString(weatherTag));
				setWind(eachWeatherResult.getString(windTag));
				setTemperature(eachWeatherResult.getString(temperatureTag));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}
