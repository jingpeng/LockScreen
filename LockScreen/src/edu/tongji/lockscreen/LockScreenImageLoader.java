package edu.tongji.lockscreen;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class LockScreenImageLoader {
	private static LockScreenImageLoader imageLoader = null;
	
	private LockScreenImageLoader(){
	}
	
	public static synchronized LockScreenImageLoader getInstance(){
		if(imageLoader == null) {
			imageLoader = new LockScreenImageLoader();
		}
		return imageLoader;
	}
	
	public List<Drawable> loadBackgroundImage(Context context){
		List<Drawable> imageList = new ArrayList<Drawable>();
		imageList.add(context.getResources().getDrawable(R.drawable.ad_bg_1));
		imageList.add(context.getResources().getDrawable(R.drawable.ad_bg_2));
		return imageList;
	}
}
