package edu.tongji.lockscreen;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.CallLog;
import android.util.Log;

public class LockScreenService extends Service {

	private static String TAG = "LockScreenService";

	BroadcastReceiver lockScreenReceiver;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {
		KeyguardManager.KeyguardLock kl;
		KeyguardManager km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
		kl = km.newKeyguardLock("IN");
		kl.disableKeyguard();

		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);

		lockScreenReceiver = new LockScreenReceiver();
		registerReceiver(lockScreenReceiver, filter);

		getContentResolver().registerContentObserver(CallLog.Calls.CONTENT_URI,
				true, new CallContentObserver(this, new Handler()));
		Uri smsUri = Uri.parse("content://sms");
		getContentResolver().registerContentObserver(smsUri, true,
				new SMSContentObserver(this, new Handler()));

		initLocationService();
		super.onCreate();
	}

	private void initLocationService() {
		LocationManager locationManager = null;
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setAltitudeRequired(false);
			criteria.setBearingRequired(false);
			criteria.setCostAllowed(false);
			criteria.setPowerRequirement(Criteria.POWER_LOW);

			String provider = LocationManager.GPS_PROVIDER;
			locationManager.requestLocationUpdates(provider, 60000, 0,
					new GPSLocationListener());
		}
	}

	class LocationRunnable implements Runnable {

		private Location location;

		@Override
		public void run() {
			BaiduWeatherService service = new BaiduWeatherService();
			service.getWeatherInfo(location);
			BaiduWeatherInfo weatherInfo = service.getWeatherInfo();
			Log.i(TAG, weatherInfo.toString());
			Message message = new Message();  
            message.obj = weatherInfo;   
            LockScreenActivity.weatherHandler.sendMessage(message); 
		}

		public void setLocation(Location location) {
			this.location = location;
		}

	}

	class GPSLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
			Log.i(TAG, location.toString());
			LocationRunnable runnable = new LocationRunnable();
			runnable.setLocation(location);
			new Thread(runnable).start();
		}

		@Override
		public void onProviderDisabled(String arg0) {

		}

		@Override
		public void onProviderEnabled(String arg0) {

		}

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

		}

	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		unregisterReceiver(lockScreenReceiver);
		super.onDestroy();
	}

}
