package edu.tongji.lockscreen;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

public class SMSContentObserver extends ContentObserver {
	private static String TAG = "SMSContentObserver";

	private int MSG_OUTBOXCONTENT = 2;

	private Context context;
	private Handler handler;

	public SMSContentObserver(Context context, Handler handler) {
		super(handler);
		this.context = context;
		this.handler = handler;
	}

	@Override
	public void onChange(boolean selfChange) {
		Log.i(TAG, "the sms table has changed");

		Uri outSMSUri = Uri.parse("content://sms/sent");

		Cursor cursor = context.getContentResolver().query(outSMSUri, null,
				null, null, "date desc");
		if (cursor != null) {

			Log.i(TAG, "the number of send is" + cursor.getCount());

			StringBuilder builder = new StringBuilder();
			while (cursor.moveToNext()) {
				builder.append(
						"发件人手机号码: "
								+ cursor.getInt(cursor
										.getColumnIndex("address")))
						.append("信息内容: "
								+ cursor.getString(cursor
										.getColumnIndex("body"))).append("\n");
			}
			cursor.close();
			handler.obtainMessage(MSG_OUTBOXCONTENT, builder.toString())
					.sendToTarget();
		}
	}

}