package edu.tongji.lockscreen;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class LockScreenReceiver extends BroadcastReceiver {

	private static String TAG = "LockScreenReceiver";

	public static boolean wasScreenOn = true;

	@SuppressWarnings("deprecation")
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			wasScreenOn = false;
			Intent intentTmp = new Intent(context, LockScreenActivity.class);
			LockScreenActivity.setLockStat(true);
			intentTmp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intentTmp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			context.startActivity(intentTmp);
		} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			wasScreenOn = true;
		} else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			KeyguardManager.KeyguardLock kl;
			KeyguardManager km = (KeyguardManager) context
					.getSystemService(Context.KEYGUARD_SERVICE);
			kl = km.newKeyguardLock("IN");
			kl.disableKeyguard();

			Intent intentTmp = new Intent(context, LockScreenActivity.class);
			LockScreenActivity.setLockStat(true);
			intentTmp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intentTmp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			context.startActivity(intentTmp);

		}
	}

}
